package com.formation.league.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="L_TEAM")
public class Team implements Serializable {

    private Long id;
    private String teamName;
    private String division;
    private Collection<Player> players;
    
    
    
    public Team() {
		super();
		// TODO Auto-generated constructor stub
	}

	
    public Team(String teamName, String division) {
        players = new HashSet();
        this.teamName = teamName;
        this.division = division;
    }

   
    @Id
    @GeneratedValue
    public Long getId() {
        return this.id;
    }

   
    public void setId(Long id) {
        this.id = id;
    }

    @OneToMany(mappedBy = "team")
    public Collection<Player> getPlayers() {
        return players;
    }
    
    public void setPlayers(Collection<Player> players) {
        this.players = players;
    }

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}
	
	public void addPlayer(Player player){
		players.add(player);
	}
    
    
}