package com.formation.league.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "L_PLAYER")
public class Player implements Serializable {

	private Long id;
	private String lastName;
	private String firstName;
	private int number;
	private String lastSpokenWords;
	private Team team;

	public Player() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Player(String lastName, String firstName, int number, String lastSpokenWords) {

		this.firstName = firstName;
		this.lastName = lastName;
		this.number = number;
		this.lastSpokenWords = lastSpokenWords;
	}

	@ManyToOne
	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	@Id
	@GeneratedValue
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String name) {
		lastName = name;
	}

	
	@Transient
	public String getLastSpokenWords() {
		return lastSpokenWords;
	}

	public void setLastSpokenWords(String lastWords) {
		lastSpokenWords = lastWords;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public int getJerseyNumber() {
		return number;
	}

	public void setJerseyNumber(int jerseyNumber) {
		this.number = jerseyNumber;
	}

	@Override
	public String toString() {
		return "Player [id=" + id + ", lastName=" + lastName + ", firstName=" + firstName + ", number=" + number
				+ ", lastSpokenWords=" + lastSpokenWords + ", team=" + team + "]";
	}

}