package com.formation.league;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.formation.league.entity.Player;
import com.formation.league.entity.Team;

/**
 * Hello world!
 *
 */
public class App {

	/** Ensemble initial de joueurs et d'équipes **/

	private static Player[] players = new Player[] {
			// name, number, last quoted statement
			new Player("Lowe", "Derek", 23, "The sinker's been good to me."),
			new Player("Kent", "Jeff", 12, "I wish I could run faster."),
			new Player("Garciaparra", "Nomar", 5, "No, I'm not superstitious at all.")

	};

	public static Team[] teams = new Team[] { new Team("Los Angeles Dodgers", "National"),
			new Team("San Francisco Giants", "National"), new Team("Anaheim Angels", "American"),
			new Team("Boston Red Sox", "American") };

	public static void main(String[] args) {

		// recupération du fabricant du gestionnaire d'entites
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("league");

		// fabrication d'un gestionnaire d'entité à partir du fabricant
		EntityManager em = emf.createEntityManager();

		// recuperation du gestionnaire de la transcation ...
		EntityTransaction et = em.getTransaction();

		// ... et démarrage de la transaction
		// et.begin();

		// enregistrement des équipes
		// for (Team team : teams) {
		// em.persist(team);
		// }

		// attribution d'une équipe à un joueur et enregistrement de ce dernier
		// for (Player player : players) {
		// player.setTeam(teams[0]);
		// teams[0].addPlayer(player);
		// em.persist(player);
		// }

		// envoi de la transaction
		// et.commit();

		//Recherche et affichage de joueurs
		for (long playerPrimaryKey = 0; playerPrimaryKey < 10; playerPrimaryKey++) {
			Player p = em.find(Player.class, playerPrimaryKey);
			if (p != null)
				System.out.println(p.toString());

		}

		//suppression d'un joueur
		Player p2 = em.find(Player.class, 6L);
		em.getTransaction().begin();
		em.remove(p2);

		System.out.println(
				"=======================================================================================================");

		//Recherche et affichage de joueurs
		for (long playerPrimaryKey = 0; playerPrimaryKey < 10; playerPrimaryKey++) {
			Player player = em.find(Player.class, playerPrimaryKey);
			if (player != null)
				System.out.println(player.toString());

		}

		//fermeture des resources
		em.close();
		emf.close();

	}
}
